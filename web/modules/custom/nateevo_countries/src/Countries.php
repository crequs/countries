<?php

namespace Drupal\nateevo_countries;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileRepositoryInterface;

/**
 * Countries service.
 */
class Countries {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;
  
  /**
   * Constructs a Countries object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $fileSystem, FileRepositoryInterface $fileRepository) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $fileSystem;
    $this->fileRepository = $fileRepository;
  }

  /**
   * Get countries from api restcountries.com
   * @param array $query
   */
  public function getCountries(array $query) {
    
    /** @var \GuzzleHttp\Client $client */
    $client = \Drupal::service('http_client_factory')->fromOptions([
      'base_uri' => 'https://restcountries.com/',
    ]);
    
    $response = $client->get('v2/all', [
      'query' => $query
    ]);

    $countries = Json::decode($response->getBody());
    
    return $countries;
  }
  
  /**
   * Import countries from array to entity country
   * @param array $countries
   */
  public function importCountries(array $countries){
    $contries_result = $this->entityTypeManager->getStorage('country')->getQuery()->count()->execute();
    if ($contries_result){
      return 'There are already countries, you can not import.';
    }
    
    if ($countries){
      foreach ($countries as $country) {
        $this->saveNewCountry($country);
      }
      return 'The importation of countries has been carried out successfully.';
    }else{
      return 'No country found to import.';
    }
  }
  
  /**
   * Save country from array.
   * @param array $country
   */
  private function saveNewCountry(array $country){
    if ($country){
      $data = [
        'title' => (isset($country['name'])?$country['name']:''),
        'field_continent' => (isset($country['region'])?$this->getContryTaxonomy($country['region']):''),
        'field_capital' => (isset($country['capital'])?$country['capital']:''),
        'field_population' => (isset($country['population'])?$country['population']:1),
        'field_area' => (isset($country['area'])?$country['area']:1),
        'field_flag' => (isset($country['flag'])?$this->getFlagFile($country['flag']):''),
      ];
      $newContry = $this->entityTypeManager->getStorage('country')->create($data);
      $newContry->save();
    }
  }
  
  /**
   * Get Id taxonomy term
   * @param text $region
   */
  private function getContryTaxonomy($region){
    $region = trim($region);
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['vid' => 'continent','name'=>$region]);
    if ($terms){
      $term = reset($terms);
      return $term->id();
    }
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->create(['vid' => 'continent','name'=>$region]);
    $term->save();
    return $term->id();
  }
  
  /**
   * Get Id flag file
   * @param type $url_flag
   */
  private function getFlagFile($url_flag){
    $image = file_get_contents($url_flag);
    if ($image){
      $name = strtolower(basename($url_flag));
      $upload_location = 'public://'.date('Y').'/'.date('m');
      $this->fileSystem->prepareDirectory($upload_location, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $image_file = $this->fileRepository->writeData($image, $upload_location.'/' . $name);
      if ($image_file) { 
        return $image_file->id();
      }else{
        return null;
      }
    }
  }

}
