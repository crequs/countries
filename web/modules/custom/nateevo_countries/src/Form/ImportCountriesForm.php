<?php

namespace Drupal\nateevo_countries\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nateevo_countries\Countries;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Cateevo countries form.
 */
class ImportCountriesForm extends FormBase {

  /**
   * @var Countries $countries_service
   */
  protected $countries_service;
  
    /**
   * Class constructor.
   */
  public function __construct(Countries $countries_service) {
    $this->countries_service = $countries_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('nateevo_countries.countries')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nateevo_countries_import_countries';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import countries'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $countries = $this->countries_service->getCountries(['fields'=>'name,capital,region,population,area,flag']);
    $result_message = $this->countries_service->importCountries($countries);
    $this->messenger()->addStatus($this->t($result_message));
    $form_state->setRedirect('<front>');
  }

}
