<?php

namespace Drupal\nateevo_countries\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for nateevo countries routes.
 */
class NateevoCountriesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
