<?php

namespace Drupal\country\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the country entity edit forms.
 */
class CountryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New country %label has been created.', $message_arguments));
      $this->logger('country')->notice('Created new country %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The country %label has been updated.', $message_arguments));
      $this->logger('country')->notice('Updated new country %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.country.canonical', ['country' => $entity->id()]);
  }

}
